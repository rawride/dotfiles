.\" compile document with:  groff -Kutf8 -ms emacs.ms -T pdf > emacs.pdf
.nr PO 2c
.nr HM 1c
.nr FM 0c
.ds CH
.ds RH -%-
.\"so macros
.de c1
.LP
.B
..
.
.de c2
.R
\ =
..
.
.de c3
.sp 0.4
..
.
.de c4
.LP
.BI
..
.
.de c5
.sp 0.2
.R
..
.
.de c6
.sp 0.6
.R
..
.
.
.
.SH
.ce 3
Emacs
(editor de texto e mais!)
.
.NH
.UL "Linha de Comando"
.sp 0.6
.
.c4
-nw
.c5
roda o emacs no terminal
.sp 1
.
.NH
.UL "Arquivos de Configuração"
.sp 0.6
.
.LP
~/.emacs.d/init.el         configurações de inicialização principais
.sp 0
~/.emacs.d/config.org  configurações de inicialização organizadas
.sp 1
.
.
.NH
.UL "Comandos Extendidos"
.sp 0.6
.
.c1
visual-line-mode
.c5
Liga/Desl. a quebra automatica de linhas no documento \ (word wrap)
.c3
.
.
.c1
global-visual-line-mode
.c5
Liga/Desl. a quebra automatica de linhas em todos os documentos \ (word wrap)
.c3
.
.
.c1
toggle-truncate-lines
.c5
Liga/Desl. a truncação de linhas no documentos \ (word wrap)
.sp 1
.
.
.NH
.UL "Teclas de Atalho"
.sp 0.6
.
.NH 2
.UL "Padrão do emacs"
.sp 1
.
.c1
C+g
.c2
\ Cancela qualquer ação
.c3
.
.c1
C+x
.c2
\ Executa ações
.c3
.
.c1
C+h
.c2
\ Menu de ajuda
.c3
.
.c1
C+o
.c2
\ Abrir arquivo
.c3
.
.c1
C+s
.c2
\ Salva arquivo
.c3
.
.c1
C+a
.c2
\ Seleciona todo o texto em tela
.c3
.
.c1
M+c
.c2
\ Capitiliza a letra a direita do cursor (capitalize-word)
.c3
.
.c1
M+u
.c2
\ Capitaliza toda a palavra a direita do cursor (upcase-word)
.c3
.
.c1
C+x C+u
.c2
\ Capitaliza toda uma seleção (upcase-region)
.c3
.
.c1
M+l
.c2
\ Descapitiliza a letra a direita do cursor (downcase-word)
.c3
.
.c1
C+x C+l
.c2
\ Descapitaliza toda uma seleção (downcase-region)
.c3
.
.c1
M+x
.c2
\ "Comando extendido" digitável no minibuffer (Meta = Alt)
.c3
.
.c1
C+x C+b
.c2
\ Vizualiza buffers
.c3
.
.c1
C+x C+c
.c2
\ Fecha o emacs
.sp 2
.
.
.NH 2
.UL "Customizadas"
.sp 1
.
.c1
C+x c
.c2
\ Simples calculadora no minibuffer
.
.
.bp
.NH 2
.UL "xah-fly-keys"
.sp 1.5
.
.
.LP
.BI "Fileira numérica"
.sp 1
.
.c1
`
.c2
\ VAZIO, NENHUMA FUNCAO ADICIONADA
.c3
.
.c1
1 2
.c2
\ PERFORMA SELECAO, HA MODIFICAR!!!!!
.c3
.
.c1
3
.c2
\ Adiciona janela horizontal abaixo (split-window-bellow)
.c3
.
.c1
4
.c2
\ Fecha todas as janelas extras mantendo somente a do cursor
.c3
.
.c1
5
.c2
\ Delete  (apaga um caractere pra direita)
.c3
.
.c1
6
.c2
\ Seleciona um paragrafo
.c3
.
.c1
7
.c2
\ Seleciona uma linha inteira
.c3
.
.c1
8
.c2
\ Seleciona uma palavra (novamente seleciona toda a linha)
.c3
.
.c1
9
.c2
\ Seleciona até o inicio de uma citação/parentesis/chave...
.c3
.
.c1
0
.c2
\ NAO INTENDI OQ FAZ,   REBINDAR!!!
.c3
.
.c1
-
.c2
\ MOVIENTO BASICO,   REBINDAR!!!
.c3
.
.c1
C+S+ +   
.c2
\ Aumenta o tomanho do texto
.c3
.
.c1
C+ -
.c2
\ Diminui o tamanho do texto
.c3
.
.
.sp 2
.LP
.BI "Fileira qwerty"
.sp 1
.
.c1
q
.c2
\ Reformata bloco/seleção
.c3
.
.c1
w
.c2
\ Insere um espaco entre palavras ou remove espacos extras entre palavras 
.c3
.
.c1
e
.c2
\ Cursor deleta uma palavra a esquerda
.c3
.
.c1
r
.c2
\ Cursor deleta uma palavra a direita
.c3
.
.c1
t
.c2
\ Inicia seleção de texto
.c3
.
.c1
y
.c2
\ Undo (Desfaz ultima ação)
.c3
.
.c1
y
.c2
\ Redo (Refaz ultima ação) necessário iniciar alguma função pra reiniciar o stack de ações 
.c3
.
.c1
u
.c2
\ Cursor se move uma palavra pra esquerda
.c3
.
.c1
j,i,k,l
.c2
\ Setas de movimentação estilo T invertido
.c3
.
.c1
o
.c2
\ Cursor se move uma palavra pra direita
.c3
.
.c1
p
.c2
\ Insere um espaco (SPC)
.c3
.
.c1
[
.c2
\ ANALIZAR!
.c3
.
.c1
]
.c2
\ ANALIZAR!
.c3
.
.c1
\(rs
.c2
\ A TESTAR!
.c3
.
.
.
.bp
.LP
.BI "Fileira asdf"
.sp 1.5
.
.c1
CapsLock
.c2
\ Modo Comando  (navegação)
.c3
.
.c1
a
.c2
\ Acessa o comando ALT+x no minibuffer
.c3
.
.c1
s
.c2
\ Pula uma linha sem mover o cursor
.c3
.
.c1
d
.c2
\ Backspace  (apaga um caractere pra esquerda)
.c3
.
.c1
f
.c2
\ Modo Inserir
.c3
.
.c1
g
.c2
\ Remove um paragrafo inteiro
.c3
.
.c1
h
.c2
\ Move o cursor pra esquerda (inicio da linha)
.c3
.
.c1
h
.c2
\ Move o cursor pra esquerda (inicio da linha)
.c3
.
.c1
;
.c2
\ Move o cursor pra direita (fim da linha)
.c3
.
.c1
'
.c2
\ Inserir - ou _ nos espaços em branco
.sp 3
.
.
.
.LP
.BI "Fileira zxcv"
.sp 1.5
.
.c1
z
.c2
\ Insere um comentário na linguagem detectada do arquivo
.c3
.
.c1
x
.c2
\ Recorta um bloco/seleção
.c3
.
.c1
c
.c2
\ Copia um bloco/seleção
.c3
.
.c1
v
.c2
\ Cola um bloco/seleção
.c3
.
.c1
b
.c2
\ A BINDAR!!!
.c3
.
.c1
n
.c2
\ Isearch  (Interactive Search) usar setas pra navegação
.c3
.
.c1
m
.c2
\ Move o cursor até o próximo parentesis a esquerda
.c3
.
.c1
,
.c2
\ Pula entre janelas (splits)
.c3
.
.c1
.
.c2
\ Move o cursor até o próximo parentesis a direita
.c3
.
.c1
/
.c2
\ Vai para o paretesis  TALVEZ REBINDAR!!
.c3
.
.
.
.bp
Atalhos com
.B "Espaço (SPC)"
.sp 1.5
.
.c1
SPC+;
.c2
\ Save file/buffer
.c3
.
.c1
SPC+\\
.c2
\ Mantem o modo de inserção
.c3
.
.c1
SPC+a
.c2
\ Seleciona todo conteudo
.c3
.
.c1
SPC+6
.c2
\ Capitaliza a 1ª letra de uma palavra em um bloco/seleção
.c3
.
.c1
SPC+9
.c2
\ iSpell checker
.c3
.
.c1
SPC+b
.c2
\ Alterna entre Maiúsculo/Minúsculo a letra à esquerda do cursor
.c3
.
.c1
SPC+h
.c2
\ Move o cursor pro inicio do texto (HOME)
.c3
.
.c1
SPC+n
.c2
\ Move o cursor pro final do texto (END)
.c3
.
.c1
SPC+g
.c2
\ Deleta todo o conteúdo da linha partindo da posição do cursor
.c3
.
.c1
SPC+c
.c2
\ Copia todo o conteúdo do buffer ou somente seleção
.c3
.
.c1
SPC+f
.c2
\ Movimenta-se entre buffers
.c3
.
.c1
SPC+m
.c2
\ Abre o editor de diretórios (Dired) na diretório do arquivo ativo
.c3
.
.c1
SPC+p
.c2
\ Re-centraliza o cursor entre top
.c3
.
.c1
SPC+r
.c2
\  Encontra e altera uma palavra (string) por outra, funciona partindo da posição do cursor até o final do buffer ou em uma seleção ativa
.c3
.
.c1
SPC+s
.c2
\ Abre um seleção entre o ponto atual do cursor e o ultimo ponto de seleção criada
.c3
.
.c1
SPC+t
.c2
\ Mostra o kill ring (lista de deleção)
.c3
.
.c1
SPC+u
.c2
\ Fecha o buffer onde o cursor está ativo
.c3
.
.c1
SPC+v
.c2
\ Cola último registro ou registros anteriores
.c3
.
.bp
Atalhos com
.B "Espaço Extendido"
.sp 2
.
.
.c1
SPC+TAB
.c2
\ Indentação, Abreviaturas...
.c3
.
.c1
SPC+,
.c2
\ Deleção, eval-buffer, Salvar e fechar buffers 
.c3
.
.c1
SPC+d
.c2
\ Inserção em geral..
.c3
.
.c1
SPC+e
.c2
\ Prefix command (definition is a keymap associating keystrokes with commands).
.c3
.
.c1
SPC+i
.c2
\ Abrir arquivos, último arquivo fechado, ibuffer, bookmarks...
.c3
.
.c1
SPC+j
.c2
\ Apropos, describes, infos, man...
.c3
.
.c1
SPC+k
.c2
\ Copias, colação, deleção, repetição, ir para linha...
.c3
.
.c1
SPC+l
.c2
\ Calendário, calculadora emacs, shell, eshell, word wrap, whitespace mode, eww
.c3
.
.c1
SPC+o
.c2
\ Macros, retangulos, converter espaços em novas linhas...
.c3
.
.c1
SPC+w
.c2
\ xref-find-definitions , xref-pop-marker-stack 
.
.
.
.bp
.NH
.B 
.UL "How to's"
.sp 1.5
.c1
Insert Literal Tab
.c2
\ C+q C+Tab
.c3
.
.c1
Insert a Newline
.c2
\ C+q Return
.c3
.
.c1
Insert a Newline during "replace"
.c2
\ C+q C+j
.c3
.
.c1
Inserir "abc" em linhas vazias
.c2
\ query-replace \  → \  C+q C+j \  → \  abc C+q C+j
.sp
.
query-replace
.I
\ (A+%)
.c5
query-replace-regexp
.I
\ (C+A+%)
.c5
.BI "Ambos pedem permissão para aplicar mudanças (query)"
.sp
.
replace-string 
.c5
replace-regexp
.c5
.BI "Ambos NÃO pedem permissão para aplicar mudanças"
.sp
.
.I "sendo:"
.sp 0.6
.c1
y 
.c2
\ Confirma alteração campo-a-campo
.c1
n 
.c2
\ Nega alteração pulando pro proximo campo
.c1
! 
.c2
\ Faz alteração pra todos os campos restantes (sem mais perguntas)
.c1
Ctrl+g 
.c2
\ Cancela a operação de mudanças
.
