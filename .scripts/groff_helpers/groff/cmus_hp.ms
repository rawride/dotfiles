.\" compile document with:  groff -Kutf8 -ms cmus_hp.ms -T pdf > cmus_hp.pdf
.nr PO 2c
.nr HM 1c
.nr FM 0c
.ds CH
.ds RH -%-
.\"so macros
.de c1
.LP
.B
..
.
.de c2
.R
\ =
..
.
.de c3
.sp 0.4
..
.
.de c4
.LP
.BI
..
.
.de c5
.sp 0.2
.R
..
.
.de c6
.sp 0.6
.R
..
.
.
.
.SH
.ce 3
CMus  
(audio player)
.sp 3
.
.
.
.LP
.B "READLINE" 
é o programa que implementa os atalhos na shell. Veja o manual para uma lista completa de comandos.
.sp 1
.
.NH
.UL "Teclas de Atalho"
.sp 0.6
.c1
A+.
.c2
\ Copia o último argumento do último comando
.c3
.
