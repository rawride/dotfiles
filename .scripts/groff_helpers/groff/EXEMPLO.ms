.TL
Titulo do documento
.AU
Autor
.AI
Instituição do Autor
.NH
Cabeçalho enumerado
.SH
Cabeçalho nao-enumerado
.PP
Isto eh um paragrafo que começa com indentação bem grande que estou testando como fica o espaçamento e distribuição do texto ao longo das linhas!
.PP
isto eh outro paragrafo com indentacao
.NH 2
cabecalho de nivel 2

.LP
paragrafo sem indentação com ".LP"
.UL "nivel 2"
com underline por ".UL"
.sp 0
e quebra de linha com ".sp" aceita argumento que define o nr. de espaçamento em Ni (inches) ou Nc (centimetros)
.br
".br" apenas quebra a linha (pula pra proxima) sem controle de espaçamento
.ce
texto centralizado com ".ce" aceita 1 argumento que define o nr. de linhs a centralizar, ".ls Xc" define o espaçamento das linhas global (no documento todo)
.
.BX "texto BOXED"
com ".BX"

"\\(rs" printa o glyph do caractere barra \(rs    "\\e" printa o caractere de escape \e
.NH 3
cabecalho de nivel 3
.LP
texto em
.B "negrito"
(bold) com ".B"

texto em
.I "italico"
com ".I"

texto em
.BI "negrito italico"
com ".BI"
