#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

# default programs
export EDITOR="emacs"
#export EDITOR="nvim"
export TERMINAL="kitty"
export BROWSER="brave"
export READER="zathura"
export FILE="ranger"
export NETCTL_DEBUG="yes"
export XDG_CONFIG_HOME="$HOME/.config"
export MPLAYER_HOME="$HOME/.config"

# aliases
alias sx="startx"
