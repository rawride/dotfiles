#   ____  ____   ___  _     
#  |  _ \|  _ \ / _ \( )___ 
#  | |_) | | | | | | |// __|
#  |  _ <| |_| | |_| | \__ \  --->  .bashrc
#  |_| \_\____/ \___/  |___/        (config file)
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Terminal prompt
PS1='[ $USER ] \$ '

# Aliases
alias ll='lsd -lAh --group-dirs first'
alias ls='ls -h --color=auto --group-directories-first'
alias fix='reset; stty sane; tput rs1; clear; echo -e "\033c"'
alias v='nvim'
alias cm='cmus'
alias za='zathura'
alias pu='pushd'
alias po='popd'
alias tl='tldr'
alias ra='ranger'
alias x='exit'
alias cp='cp -iv'
alias cpr='rsync -ah --progress'
alias mv='mv -iv'
alias rm='rm -iv'
alias frm='\rm -rfv'  ## frm = force rm
alias le='bat'
alias se='script_editor.sh'  ## search w/ fzf the  .scipts , .config  dirs & open files w/ nvim
alias fe='file_editor.sh'    ## search w/ fzf the current directory & open files w/ nvim
alias mm='rofi_man.sh'      ## searches the man pages with rofi, output selected program in .pdf and delete pdf after closure

# Paths
PATH=$PATH:~/.config
PATH=$PATH:~/.scripts
PATH=$PATH:~/.scripts/groff_helpers/helpers
PATH=$PATH:~/.themes
PATH=$PATH:~/.icons

# Readline customization
complete -cf sudo

# don't put duplicate lines or lines starting with spaces in the history
HISTCONTROL=ignoreboth

# history lenght
HISTSIZE=1000

# history file size
HISTFILESIZE=2000

# [ -f ~/.fzf.bash ] && source ~/.fzf.bash
